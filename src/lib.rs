use std::{collections::HashMap, rc::Rc};

use yew::prelude::*;

#[derive(Debug, Clone, PartialEq)]
struct Counters {
    data: HashMap<String, u32>,
}

#[derive(Debug, Clone, PartialEq)]
enum CountersMessage {
    Increment(String),
    Reset(String),
    ResetAll,
}

impl Reducible for Counters {
    type Action = CountersMessage;

    fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
        let counters = match action {
            CountersMessage::Increment(id) => {
                let counter = match self.data.get(&id) {
                    Some(counter) => *counter,
                    None => 0,
                };

                let mut data = self.data.clone();
                data.insert(id, counter+1);

                Counters { data }
            }
            CountersMessage::Reset(id) => {
                let mut data = self.data.clone();
                data.insert(id, 0);

                Counters { data }
            }
            CountersMessage::ResetAll => {
                let mut data = self.data.clone();
                data.clear();

                Counters { data }
            }
        };

        Rc::new(counters)
    }
}

type CounterContext = UseReducerHandle<Counters>;

#[function_component]
fn Group() -> Html {
    let onincrement = {
        let ctx = use_context::<CounterContext>().unwrap();

        Callback::from(move |id| {
            log::info!("Triggered increment of ID: {}", id);
            ctx.dispatch(CountersMessage::Increment(id));
        })
    };

    let onreset = {
        let ctx = use_context::<CounterContext>().unwrap();

        Callback::from(move |id| {
            log::info!("Triggered reset of ID: {}", id);
            ctx.dispatch(CountersMessage::Reset(id));
        })
    };

    let cb_reset_all = {
        let ctx = use_context::<CounterContext>().unwrap();

        Callback::from(move |_| {
            ctx.dispatch(CountersMessage::ResetAll);
        })
    };

    html! {
        <>
            <button onclick={cb_reset_all}>{"Reset all"}</button>
            <Button id="btn-1" onincrement={onincrement.clone()} onreset={onreset.clone()} />
            <Button id="btn-2" {onincrement} {onreset} />
        </>
    }
}

#[derive(Debug, Clone, PartialEq, Properties)]
struct ButtonProps {
    id: String,
    onincrement: Callback<String>,
    onreset: Callback<String>,
}

/// 
/// A Button to trigger increments or resets
/// 
/// Only owns an ID to propagate events
#[function_component]
fn Button(
    ButtonProps {
        id,
        onincrement,
        onreset,
    }: &ButtonProps,
) -> Html {
    let ctx = use_context::<CounterContext>().unwrap();
    let value = ctx.data.get(id).cloned().unwrap_or(0);

    let btn_increment = {
        let id = id.clone();
        let onincrement = onincrement.clone();
        let onclick = Callback::from(move |_| {
            onincrement.emit(id.clone());
        });

        html! {
            <button {onclick}>{"Increment"}</button>
        }
    };

    let btn_reset = {
        let id = id.clone();
        let onreset = onreset.clone();
        let onclick = Callback::from(move |_| {
            onreset.emit(id.clone());
        });

        html! {
            <button {onclick}>{"Reset"}</button>
        }
    };

    //
    html! {
        <>
            <div>
                {btn_increment}
                {btn_reset}
            </div>
            <div>
                {value}
            </div>
        </>
    }
}

///
/// Main App Component
/// 
/// Provides Context data
/// 
#[function_component]
pub fn App() -> Html {
    let context = use_reducer(|| Counters {
        data: HashMap::new(),
    });
    html! {
        <ContextProvider<CounterContext> {context}>
            <Group />
        </ContextProvider<CounterContext>>
    }
}
